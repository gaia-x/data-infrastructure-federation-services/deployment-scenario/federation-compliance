import { Module } from '@nestjs/common'
import { ApiKeyGuard } from './apikey.guard'

@Module({
  imports: [],
  controllers: [],
  providers: [ApiKeyGuard]
})
export class AuthModule {}
