import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";

@Injectable()
export class ApiKeyGuard implements CanActivate {
    key = process.env.API_KEY_COMPLIANCE
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = context.switchToHttp().getRequest()
        const key = req.headers['X-API-KEY'] ?? req.headers['x-api-key']
        if (!key) {
            throw new UnauthorizedException('API key is missing.')
        }
        return key === this.key
    }
}