import { BadRequestException, ForbiddenException, Injectable, Logger, NotFoundException, NotImplementedException, ServiceUnavailableException, UnprocessableEntityException } from '@nestjs/common';
import {AllRulesDescription, CredentialSubjectDto, InitiateSession, RulesDescription, Session, testResult,VerifiableCredentialDto,VerifiablePresentationDto,VerificationPayload } from './dto';
import { firstValueFrom } from 'rxjs';
import { get_rules, get_all_rules } from './utils';
import { HttpService } from '@nestjs/axios';

import { credentialProvider } from './interface/credential.interface';
import { jsonldProvider } from './provider/jsonld.provider';
import { JWTProvider } from './provider/jwt.provider';
import { sdJWTProvider } from './provider/sd-jwt.provider';
import {  v4 } from 'uuid'



const  logger = new Logger("MainService")

@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {
    this.watchSession()
  }
  registryURL = process.env.REGISTRY_URL || "https://registry.abc-federation.dev.gaiax.ovh"
  requests:Session[] = []
  getAllRules():AllRulesDescription {
    return get_all_rules()
  }

  initiateSession():InitiateSession {
    const domain = process.env.DOMAIN
    const challenge = v4()
    const date = new Date()
    const expirationDate = date.setMinutes(date.getMinutes() + 5)
    this.requests.push( {
      challenge:challenge,
      ttl: expirationDate
    })
    return {
      challenge:challenge,
      domain:domain
    }
  }

  getRules(id:string, type:string):RulesDescription {
    console.log(type)
    console.log(id)
    let rules = get_rules(type)
    let rule = rules.find(x=> x.id === id)
    if(rule === undefined){
      throw new NotFoundException()
    }
    return rule
  }

  getProvider(contentType: string, type: string): credentialProvider {
    if(contentType == "application/vp+ld+json" || contentType== "application/json") {
        return new jsonldProvider(type)
    }
    if(contentType =="application/vp+jwt") {
      return new JWTProvider(type)
    }
    if (contentType == "application/vp+sd-jwt") {
      return new sdJWTProvider(type)
    } else {
      console.error("detected content type is ", contentType)
      throw new UnprocessableEntityException("Content type is not correct or not yet implemented")
    }
  }

  checkRequest(contentType:string, payload: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>) {
    if(process.env.MODE==="PRODUCTION") {
      if(contentType == "application/vp+ld+json" || contentType== "application/json") {
        this.checkProperties(payload)
      } else return
    } else return 
    
  }

 async watchSession() {
    while(true) {
      logger.log("Begin session cleaning")
      var date = new Date()
      this.requests.map(x => {
        logger.log(x)
        logger.log(date.getTime())
        if(date.getTime()>x.ttl) {
          logger.log("Removing ", x)
          this.requests.splice(this.requests.indexOf(x),1)
        }
      })
      await this.delay(600000)
      // await this.delay(3000)
    }
  }

  async checkRules( verificationPayload:VerificationPayload, type:string):Promise<void>{
    let rules = get_rules(type)

    logger.log(`Applying rules ${rules} on VP`)
    let validationResults:testResult[] = []
    await rules.map(async rule => {
      verificationPayload.rules.push(rule.id)
      logger.log(`Initiating Rule check for rule ${rule.id}`)
      if(rule.required) {
        if(rule.required.registry) {
          verificationPayload[`${rule.id}:registryURL`]= this.registryURL + rule.required.path
        }
      }
    })
    logger.log(`Sending ${JSON.stringify(verificationPayload)} to opal`)
    validationResults = await this.sendToOPA(verificationPayload)
    logger.log(`receiving response from opal  ${JSON.stringify(validationResults)}`)
    return this.reformatResult(validationResults)
}

  private reformatResult(validationResults:testResult[]):void{
    validationResults.map(result => {
      if(result.result ==false) {
          logger.error("One test has failed: ", result)
          throw new ForbiddenException(validationResults)  
      }
    })
    logger.log("All test are correct")
    return
  }

  private reformatResultSignature(signatureResults:testResult[]):void{
    signatureResults.map(result => {
      if(result.result ==false) {
          logger.error("One signature test has failed: ", result)
          throw new ForbiddenException(signatureResults)
      }
    })
    return
  }


  private async sendToOPA(payload: VerificationPayload): Promise<testResult[]> {
    try {
      let request = await firstValueFrom(this.httpService.post("http://localhost:8181/v1/data/gaiax/core/abc_checker", {input: payload}))
      return request.data.result
    } catch (e){
      throw new ServiceUnavailableException("Error: Connexion refused to local OPA instance")
    }   
  }

  private checkProperties(vp:VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>) {
    if(!vp.proof.challenge || !vp.proof.domain) {
      throw new NotFoundException("Missing Property : challenge or domain in the Verifiable Presentation")
    } else {
      if(vp.proof.domain !== process.env.DOMAIN) {
        throw new BadRequestException("Domain is incorrect")
      }
      let currentChallenge= this.requests.find(x=> x.challenge===vp.proof.challenge)
      let currentChallengeIndex = this.requests.indexOf(currentChallenge)
      console.log(currentChallengeIndex)
      let date = new Date()
      if(!currentChallenge) {
        throw new BadRequestException("Associated challenge not found in the cache, session may have been deleted due to inactivity for too long")
      } else {
        if(date.getTime()>currentChallenge.ttl) {
          this.requests.splice(currentChallengeIndex,1)
          throw new BadRequestException("Challenge is expired")
        }
        if(currentChallenge.challenge!==vp.proof.challenge) {
          throw new BadRequestException("Challenge is incorrect")
        }
        this.requests.splice(currentChallengeIndex,1)
        return
      }
    }
  }


  private delay(ms:number) {
    return new Promise(resolve => setTimeout(resolve,ms))
  }
}
