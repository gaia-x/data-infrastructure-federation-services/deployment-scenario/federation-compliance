import {  CredentialSubjectDto, VerifiableCredentialDto, ComplianceGaiaX, ComplianceCredentialSubject } from '../dto'
import  * as crypto from 'crypto'
import { BadRequestException, ConflictException, ForbiddenException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common'
import * as jose from 'jose'
import * as jsonld from 'jsonld'
import { HttpService } from '@nestjs/axios'
import { firstValueFrom } from 'rxjs'
import { Logger } from '@nestjs/common'
import { uuid } from 'uuidv4'
const  logger = new Logger("Signature Service")




export interface Verification {
  protectedHeader: jose.CompactJWSHeaderParameters | undefined
  content: string | undefined
}

@Injectable()
export class SignatureService {
  constructor(private httpService: HttpService) {}

  async createComplianceCredential(
    complianceGX: ComplianceGaiaX<ComplianceCredentialSubject> | VerifiableCredentialDto<CredentialSubjectDto>,
    type:string
  ): Promise<VerifiableCredentialDto<CredentialSubjectDto>> {
    try {
        let vcId; 
        let subjectID;
        let typeCompliance;
        let credentialSubject;
        let participantCredential;
        let resolvePromises:Promise<any>[] = []
        if((complianceGX as ComplianceGaiaX<ComplianceCredentialSubject>).credentialSubject.length) { 
          
        } else {
          if(complianceGX.credentialSubject['gx:type']==='gx:LegalParticipant' && type==='Participant') {
            subjectID = (complianceGX as VerifiableCredentialDto<CredentialSubjectDto>).credentialSubject.id
            typeCompliance='aster-conformity:ComplianceParticipant'
        }
        if(complianceGX.credentialSubject['gx:type']==='gx:ServiceOffering' && type==='ServiceOffering') {
          subjectID = (complianceGX as VerifiableCredentialDto<CredentialSubjectDto>).credentialSubject.id
          typeCompliance='aster-conformity:ComplianceServiceOffering'
      }
        }
        


      
      // let signedComplianceCredential= await firstValueFrom(this.httpService.put(process.env.USER_AGENT + "/api/vc-request",credentialSubject, {
      //   headers: {
      //     "X-API-KEY":process.env.USER_AGENT_API_KEY
      //   }
      // }))
      // console.log(signedComplianceCredential)
      return credentialSubject
    } catch (e) {
      if(e.response.status===401) {
        throw new UnauthorizedException("Could not connect to user agent: Missing api key or api key invalid")
      }
      if(e.response.status==404) {
        throw new NotFoundException("Cannot connect to the lifecycle configuration URL, check if the url is correct or if the service is running")
      } else {
        console.log("error is ", e.response)
      }
    }
  }








}
