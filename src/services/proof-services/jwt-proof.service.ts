import  { createHash } from 'crypto'
import * as jose from 'jose'
import * as jsonld from 'jsonld'
import { DIDDocument, Resolver } from 'did-resolver'
import * as web from 'web-did-resolver'
import { BadRequestException, ConflictException, Injectable, Logger, UnprocessableEntityException } from '@nestjs/common'
import { DecodedVC, DecodedVP, testResult } from '../../dto'
import { jwtDecode } from 'jwt-decode'
import { cp } from 'fs'



const webResolver = web.getResolver()
const resolver = new Resolver(webResolver)
const logger = new Logger('Signature service')

export class jwtProofService {


    public async validate(
        selfDescriptionCredential: string
      ): Promise<any> {
        try {
          const publicKeyJwk  = await this.getPublicKeys(selfDescriptionCredential)
          let decodedVC:DecodedVC | DecodedVP = jwtDecode(selfDescriptionCredential)
          let elementWithoutDisclosure = selfDescriptionCredential.split("~")
          try {
            await jose.jwtVerify(elementWithoutDisclosure[0], publicKeyJwk)
          } catch(e) {
            return {
                message:"Signature is incorrect",
                result: false,
                rule_id: 'Signature Rule',
                id: this.getId(decodedVC)
              }
          }
          
          logger.log('signature validated')
         
          return {
            message:"Signature is correct",
            result: true,
            rule_id: 'Signature Rule',
            id: this.getId(decodedVC)
          }
        } catch(e) {
        let decodedVC:DecodedVC | DecodedVP = jwtDecode(selfDescriptionCredential)
          return {
            message:e.response.message,
            result: false,
            rule_id: 'Signature Rule',
            id: this.getId(decodedVC)
          }
        }
      }

   


      getId(selfDescription:any) {
        if(selfDescription.vp) {
            if(!selfDescription.vp["@id"] || !selfDescription.vp.id) {
                return "No id available for element of type Verifiable Presentation" 
            } else {
                if(selfDescription.vp.id) {
                    return selfDescription.vp.id
                } 
                if(selfDescription.vp["@id"]) {
                    return selfDescription.vp["@id"]
                } 
            }
        } 
        if(selfDescription.vc) {
            if(selfDescription.vc.id) {
                return selfDescription.vc.id
            } 
            if(selfDescription.vc["@id"]) {
                return selfDescription.vc["@id"]
            } 
            else {
              return "No id available for element of type " + selfDescription.vc.credentialSubject.type
            }
        }
        if(selfDescription.id) {
          return selfDescription.id
        }
        if(selfDescription["@id"]) {
          return selfDescription["@id"]
        }
        
      }

      getType(selfDescription:any) {
        if(selfDescription.type) {
            return selfDescription.type
        } 
        if(selfDescription["@type"]) {
            return selfDescription["@type"]
        } 

      }

      getIssuer(selfDescription:any) {
        if(selfDescription.iss) {
          return selfDescription.iss
        } 
        if(selfDescription.issuer) {
          if(!selfDescription.issuer.id) {
            return selfDescription.issuer
          } else return selfDescription.issuer.id
        } 
      }

      private async loadDDO(did: string): Promise<any> {
        let didDocument
        try {
          didDocument = await this.getDidWebDocument(did)
        } catch (error) {
          throw new ConflictException(`Could not load document for given did:web: "${did}"`)
        }
        if (!didDocument?.verificationMethod || didDocument?.verificationMethod?.constructor !== Array)
          throw new ConflictException(`Could not load verificationMethods in did document at ${didDocument?.verificationMethod}`)
    
        return didDocument || undefined
      }

      private async getDidWebDocument(did: string): Promise<DIDDocument> {
        try {
            const doc = await resolver.resolve(did)
            if(!doc.didDocument) {
                throw new UnprocessableEntityException("Could not resolve did: " + did + "This may be due to a non did url or an expired certificate")
            }
            return doc.didDocument
        } catch (e) {
            throw e
        }
        
        
      }

      public async getPublicKeys(selfDescriptionCredential) {
        try {
            let decodedCredential = jwtDecode(selfDescriptionCredential)
          if (!this.getIssuer(decodedCredential)) {
            console.log("throwing")
            throw new ConflictException('Missing field: Issuer uin credential')
          }
          const { verificationMethod, id } = await this.loadDDO(this.getIssuer(decodedCredential))
          if(!verificationMethod) {
            throw new ConflictException("Could not resolve DID Document")
          }
          const jwk = verificationMethod.find(method =>  method.id.startsWith(id))
          if (!jwk) throw new ConflictException(`Missing field: Verification Method in the Verifiable Credenial`)
          const { publicKeyJwk } = jwk
          if (!publicKeyJwk) throw new ConflictException(`Could not load JWK for ${verificationMethod}`)
          return  await jose.importJWK(publicKeyJwk)
        } catch(e) {
          throw(e)
        }

      }






    }

