import { PipeTransform } from "@nestjs/common";

import { VerifiablePresentationDto, VerificationPayload, VerifiableCredentialDto, CredentialSubjectDto, ComplianceCredentialSubject, ComplianceGaiaX} from '../dto'


export interface credentialProvider  {

    parse(vp: string | VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>):Promise<VerificationPayload>
    verify(vp: string | VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>):Promise<void>
    createComplianceCredential(complianceGX:ComplianceGaiaX<ComplianceCredentialSubject> | VerifiableCredentialDto<CredentialSubjectDto>,type:string, vcid?:string, credentialSubjectID?:string ): Promise<any>

}