import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SignatureService } from './services/signature.service';
import { jsonldProofService } from './services/proof-services/jsonld-proof.service';
import { ApiKeyGuard } from './auth/apikey.guard';

@Module({
  imports: [HttpModule, ServeStaticModule.forRoot({
    rootPath: join(__dirname, 'static'),
    serveRoot: process.env['APP_PATH'] ? process.env['APP_PATH'] : '/',
    exclude: ['/api*']
  }),],
  controllers: [AppController],
  providers: [AppService,  SignatureService, jsonldProofService, ApiKeyGuard],
  exports: []
})
export class AppModule {}
