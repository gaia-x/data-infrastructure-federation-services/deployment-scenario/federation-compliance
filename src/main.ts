import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupSwagger } from './swagger';
import * as dotenv from 'dotenv';
import { importCertChain, get_rules } from './utils';
import * as bodyParser from "body-parser";


async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create(AppModule);
  setupSwagger(app)
  app.enableCors()
  app.use(bodyParser.text({ type: 'application/*+jwt' }))
  app.use(bodyParser.text({ type: 'application/*+sd-jwt' }))
  importCertChain()
  await app.listen(3003);
}
bootstrap();
