import { ComplianceCredentialSubject, CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto, VerificationPayload } from "src/dto";
import { ComplianceGaiaX
 } from "src/dto";
import { credentialProvider } from "src/interface/credential.interface";
import { BadRequestException, Logger } from "@nestjs/common";
import { get_rules, getDID } from "src/utils";
import { UnprocessableEntityException } from "@nestjs/common";
import * as jp from 'jsonpath'
import { jsonldProofService } from "src/services/proof-services/jsonld-proof.service";
import { testResult } from "src/dto";
import { HttpService } from "@nestjs/axios";
import { firstValueFrom } from "rxjs";
import { NotFoundException } from "@nestjs/common";
import { uuid } from "uuidv4";
import e from "express";

export class jsonldProvider implements credentialProvider {
    constructor(private type:string) {}
    proofService = new jsonldProofService()
    httpService = new HttpService
    userAgentApiKey= process.env.USER_AGENT_API_KEY
    logger = new Logger("jsonldProvider")
    async parse(vp:VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>): Promise<VerificationPayload> {
        this.logger.log("Begin signature verification")
        await this.verify(vp)
        this.logger.log("parsing for jsonld")
        let rules = get_rules(this.type)
          try {
            let verificationPayload:VerificationPayload = {
              rules:[],
            }
            let requirements = []
            
            rules.map(rule => {
              if(rule.required){
                rule.required.type.map(type => {
                  if(!requirements.includes(type)) {
                      requirements.push(type)
                  }
              })
              }     
            })
            console.log(requirements)
            let promises: Promise<void>[] = []
            requirements.map(async requirement => {
              promises.push(this.findVC(vp,requirement, verificationPayload))
            })
            await Promise.all(promises)
            return verificationPayload
          } catch(e) {
            this.logger.log(e)
            throw (e)
          }
    }

    async verify(vp: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>> ): Promise<void> {
      let signatureResults: testResult[] = []
      let signaturePromises:Promise<testResult>[] = []
      signaturePromises.push(this.proofService.validate(vp))
      this.logger.log("beginning vc verifiaction")
      try {
      console.log(vp.verifiableCredential )
       await firstValueFrom(await this.httpService.post(process.env.VERIFIER + "/api/rules", vp.verifiableCredential))  
      } catch(e) {
        console.error(e.response.data)
        if(e.response.data.statusCode===403) {
          await this.checkErrorVerifier(e.response.data.message)
        }
        if(e.response.data.statusCode===422) {
          throw new UnprocessableEntityException(e.response.data.message)
        }
        throw e
      }
       
      signatureResults = await Promise.all(signaturePromises)
      await this.checkResult(signatureResults)
    }

    async checkResult(signatureResult: testResult[]):Promise<void> {
      let failedSignature: string[] = []
        signatureResult.map(x => {
          if(x.result===false) {
            failedSignature.push(x.id)
          }
        })
        console.log(failedSignature)
        if(failedSignature.length) throw new BadRequestException("Signature is invalid for credential of id : " + failedSignature)
     }

     async checkErrorVerifier(verifierResults: any[]):Promise<void> {
      let failedRules: any[] = []
        verifierResults.map(x => {
          if(x.is_valid===false) {
            failedRules.push({
              id:x.vc_id,
              invalidRules: x.invalidRules
            })
          }
        })
        console.error(failedRules)
        if(failedRules.length) throw new BadRequestException("Verifier has returned an error for vc of ID " + failedRules.map(x=>x.id) + " here is a list of all errors : " + failedRules.map(x=>JSON.stringify(x.invalidRules)))
     }

    async findVC(vp, requirement, verificationPayload) {
        try {
          let result = await new Promise<void>((resolve, reject) => {
            let searchForTypeInVC = jp.query(vp, '$.verifiableCredential[*].credentialSubject.type')
            let searchForTypeInVCld =jp.query(vp,'$.verifiableCredential[*].credentialSubject["@type"]')
            let searchForTypeInArray = jp.query(vp, '$.verifiableCredential[*].credentialSubject[*].type')
            if(searchForTypeInVC.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type ${requirement}`)
              let vc = jp.query(vp, `$.verifiableCredential[?(@.credentialSubject.type==="${requirement}")]`)[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else if(searchForTypeInVCld.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type ${requirement}`)
              let vc = jp.query(vp, `$.verifiableCredential[?(@.credentialSubject["@type"]==="${requirement}")]`)[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else if(searchForTypeInArray.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type in searchForTypeInArray "${requirement}"`)
              let ArrayVC = jp.query(vp, `$.verifiableCredential[?(@.credentialSubject[0])]`)
              let vc = ArrayVC.filter(x => x.credentialSubject.some(subject => subject.type === requirement))[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else {
              reject("unprocessableEntity")
            }
          })
        } catch(e) {
          console.log(e)
          throw new UnprocessableEntityException(`Missing VC of type ${requirement}`)
        }
        
    
      }

    async createComplianceCredential(
      complianceGX: ComplianceGaiaX<ComplianceCredentialSubject>,
      type:string,
      vcID?:string,
      credentialSubjectID?:string
    ) {
      let subjectID;
      let typeCompliance;
      let credentialPromise:Promise<any>;
      complianceGX.credentialSubject.map(async (x) => {
        if(x['gx:type']==='gx:LegalParticipant' && type==='Participant') {         
          credentialPromise = this.createMembershipAsterX(x)
      }
        if(x['gx:type']==='gx:ServiceOffering' && type==='ServiceOffering') {
          credentialPromise = this.createServiceOfferingAsterX(x, vcID, credentialSubjectID)
      }
      })
      return await Promise.resolve(credentialPromise)
    }

    private async createMembershipAsterX(participantCompliance) {
      try {
        let uriEncoded = encodeURIComponent(participantCompliance.id)
        let participantVC = (await firstValueFrom(this.httpService.get(process.env.VERIFIER + "/api/resolve/" + uriEncoded))).data
        let vcId = process.env.USER_AGENT + "/organization-json/" + uuid()  + "/data.json"
        let subjectID = participantVC.issuer
        let credentialSubject={
            "@id": vcId,
            "@type": "vcard:Organization",
            "vcard:title": "Aster-X Membership",
            "vcard:hasMember": {
              "@type": "vcard:Individual",
              "vcard:fn": participantVC.credentialSubject["gx:legalName"],
              "aster-conformity:hasDid": subjectID
        }
      }
      this.logger.log("Storing " + credentialSubject)
      await firstValueFrom(this.httpService.post(process.env.USER_AGENT+ "/api/store_object/organization", {objectjson:credentialSubject}, {headers: {"X-API-KEY":this.userAgentApiKey}}))
      this.logger.log("Signing the VC")
      let signedCredential = (await firstValueFrom(this.httpService.put(process.env.USER_AGENT + "/api/vc-request", credentialSubject,
      {headers: {"X-API-KEY":this.userAgentApiKey}} ))).data
      return signedCredential
    
      
      } catch(e) {
        console.error(e)
        if(e.response.status===404) {
          console.error(e.response)
          throw new NotFoundException("Participant credential in Gaia-X Compliance is not resolvable")
        } 
        if(e.response.status===400) {
          console.error("throwing bad request")
          throw new BadRequestException(e.response.data.msg)
        } else {
          console.error(e)
        }
      }
    }


    private async createServiceOfferingAsterX(serviceCompliance, vcID, credentialSubjectID) {
      try {
        let uriEncoded = encodeURIComponent(serviceCompliance.id)
        let serviceOfferingVC = (await firstValueFrom(this.httpService.get(process.env.VERIFIER + "/api/resolve/" + uriEncoded))).data
        
      if(vcID && credentialSubjectID) {
        
        let credentialSubject={
          "@id": credentialSubjectID,
          "@type": "aster-conformity:AsterXCompliance",
          "aster-conformity:hasGaiaXCompliance":serviceCompliance.id,
          "aster-conformity:serviceName":serviceOfferingVC.credentialSubject["gx:name"]
        }

        let toBeSigned = {
          "@context":[
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
            "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"
         ],
          "@id":vcID,
          issuer:getDID(),
          credentialSubject:credentialSubject,
          type:["VerifiableCredential"]
        }

        let asterXCompliance = (await firstValueFrom(this.httpService.put(process.env.VC_ISSUER+"/api/v0.9/sign", toBeSigned ))).data
        return asterXCompliance
      } else {
        let credentialSubjectID = process.env.USER_AGENT + "/aster-x-service-offering-json/" + uuid()  + "/data.json"
        let credentialSubject={
            "@id": credentialSubjectID,
            "@type": "aster-conformity:AsterXCompliance",
            "aster-conformity:hasGaiaXCompliance":serviceCompliance.id,
            "aster-conformity:serviceName":serviceOfferingVC.credentialSubject["gx:name"]
      }
      await firstValueFrom(this.httpService.post(process.env.USER_AGENT+ "/api/store_object/aster-x-service-offering", {objectjson:credentialSubject}, {headers: {"X-API-KEY":this.userAgentApiKey}}))
      this.logger.log("Signing the VC")
      let signedCredential = (await firstValueFrom(this.httpService.put(process.env.USER_AGENT + "/api/vc-request", credentialSubject,
      {headers: {"X-API-KEY":this.userAgentApiKey}} ))).data
        return signedCredential
      }
     
    
      
      } catch(e) {
        console.error(e)
        if(e.response.status===404) {
          console.error(e.response)
          throw new NotFoundException("Participant credential in Gaia-X Compliance is not resolvable")
        } 
        if(e.response.status===400) {
          console.error("throwing bad request")
          throw new BadRequestException(e.response.data.msg)
        } else {
          console.error(e)
        }
      }
    }
}
