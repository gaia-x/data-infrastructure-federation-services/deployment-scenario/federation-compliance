import { CredentialSubjectDto, DecodedVC, DecodedVP, testResult, VerifiableCredentialDto, VerifiablePresentationDto, VerificationPayload } from "src/dto";
import { credentialProvider } from "src/interface/credential.interface";
import { BadRequestException, ConflictException, Logger, UnprocessableEntityException, NotFoundException} from "@nestjs/common";
import { firstValueFrom } from "rxjs";
import { jwtDecode } from "jwt-decode";
import { jwtProofService } from "src/services/proof-services/jwt-proof.service";
import { get_rules } from "src/utils";
import * as jp from 'jsonpath'
import { HttpService } from "@nestjs/axios";
import { uuid } from "uuidv4";

export class JWTProvider implements credentialProvider {
    constructor(private type:string) {}
    proofService = new jwtProofService()
    httpService = new HttpService()
    userAgentApiKey= process.env.USER_AGENT_API_KEY
    logger = new Logger("jProvJWTder")
    async parse(vp:string): Promise<VerificationPayload> {
        this.logger.log("Checking credential validity")
        await this.verify(vp)

        this.logger.log("parsing for jwt")
        let decodedVP:DecodedVP = jwtDecode(vp)
        let rules = get_rules(this.type)
        let verificationPayload:VerificationPayload = {
            rules:[]
          }
        let decodedVCs = {
            verifiableCredential: []
        }
        decodedVP.vp.vc.map(x=> {
            let decodedVC:DecodedVC = jwtDecode(x)
            let parsedVC = {...decodedVC.vc}
            if(parsedVC.issuer != decodedVC.iss) {
                throw new ConflictException("Error when matching issuer and iss field in VC of type " + parsedVC.credentialSubject.type)
            } else {
                decodedVCs.verifiableCredential.push(parsedVC)
            }   
          })
          let requirements = []
          rules.map(rule => {
            if(rule.required){
              rule.required.type.map(type => {
                if(!requirements.includes(type)) {
                    requirements.push(type)
                }
            })
            }     
          })
          let promises: Promise<void>[] = []
            requirements.map(async requirement => {
              promises.push(this.findVC(decodedVCs, requirement, verificationPayload))
            })
            await Promise.all(promises)
        return verificationPayload
    }

    async verify(vp: string ): Promise<void> {
    let signatureResults: testResult[] = []
      let signaturePromises:Promise<testResult>[] = []
      let decodedVP:DecodedVP = jwtDecode(vp) 
      if(!decodedVP.vp) {
        throw new BadRequestException("Submitted JWT is not a Verifiable Presentation of type application/vp+jwt")
      }
      signaturePromises.push(this.proofService.validate(vp))
      decodedVP.vp.vc.map(x => {
        signaturePromises.push(this.proofService.validate(x))
      })
      signatureResults = await Promise.all(signaturePromises)
      await this.checkResult(signatureResults)


    }


    async checkResult(signatureResult: testResult[]):Promise<void> {
        let failedSignature: string[] = []
          signatureResult.map(x => {
            if(x.result===false) {
              failedSignature.push(x.id)
            }
          })
          console.log(failedSignature)
          if(failedSignature.length) throw new BadRequestException("Signature is invalid for credential of id : " + failedSignature)
       }


       async findVC(vcs, requirement, verificationPayload) {
        try {
          let result = await new Promise<void>((resolve, reject) => {
            let searchForTypeInVC = jp.query(vcs, '$.verifiableCredential[*].credentialSubject.type')
            let searchForTypeInArray = jp.query(vcs, '$.verifiableCredential[*].credentialSubject[*].type')
            if(searchForTypeInVC.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type ${requirement}`)
              let vc = jp.query(vcs, `$.verifiableCredential[?(@.credentialSubject.type==="${requirement}")]`)[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else if(searchForTypeInArray.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type in searchForTypeInArray "${requirement}"`)
              let ArrayVC = jp.query(vcs, `$.verifiableCredential[?(@.credentialSubject[0])]`)
              let vc = ArrayVC.filter(x => x.credentialSubject.some(subject => subject.type === requirement))[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else {
              reject("unprocessableEntity")
            }
          })
        } catch(e) {
          console.log(e)
          throw new UnprocessableEntityException(`Missing VC of type ${requirement}`)
        }
        
    
      }

      async createComplianceCredential(
        complianceGX: any,
        type:string
      ) {

        let credentialPromise:Promise<any>;


        if(complianceGX.credentialSubject['gx:type']==='gx:LegalParticipant' && type==='Participant') {
          credentialPromise = this.createMembershipAsterX(complianceGX.credentialSubject)
         
      }

      return await Promise.resolve(credentialPromise)

      }


      private async createMembershipAsterX(participantCompliance) {
        try {
          let uriEncoded = encodeURIComponent(participantCompliance.id)
          console.log(participantCompliance)
          let participantVC = (await firstValueFrom(this.httpService.get(process.env.VERIFIER + "/api/resolve/" + uriEncoded))).data
          let vcId = process.env.USER_AGENT + "/organization-json/" + uuid()  + "/data.json"
          let subjectID = participantVC.issuer
          let credentialSubject={
              "@id": vcId,
              "@type": "vcard:Organization",
              "vcard:title": "Aster-X Membership",
              "vcard:hasMember": {
                "@type": "vcard:Individual",
                "vcard:fn": participantVC.credentialSubject["gx:legalName"],
                "aster-conformity:hasDid": subjectID
          }
        }
        this.logger.log("Storing " + credentialSubject)
        await firstValueFrom(this.httpService.post(process.env.USER_AGENT+ "/api/store_object/organization", {objectjson:credentialSubject}, {headers: {"X-API-KEY":this.userAgentApiKey}}))
        this.logger.log("Signing the VC")
        let signedCredential = (await firstValueFrom(this.httpService.put(process.env.USER_AGENT + "/api/vc-request", credentialSubject,
        {headers: {"X-API-KEY": this.userAgentApiKey}} ))).data
        return signedCredential
      
        
        } catch(e) {
          console.error(e)
          console.error("header used was: " + JSON.stringify({headers: {"X-API-KEY": this.userAgentApiKey}}))
          if(e.response.status===404) {
            throw new NotFoundException("Participant credential in Gaia-X Compliance is not resolvable")
          } 
          if(e.response.status===400) {
            console.error("throwing bad request")
            throw new BadRequestException(e.response.data.msg)
          } else {
            console.error(e)
          }
        }
      }
}