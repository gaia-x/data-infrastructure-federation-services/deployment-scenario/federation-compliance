import { CredentialSubjectDto, DecodedVC, DecodedVP, testResult, VerifiableCredentialDto, VerifiablePresentationDto, VerificationPayload } from "src/dto";
import { credentialProvider } from "src/interface/credential.interface";
import { BadRequestException, ConflictException, Logger, UnprocessableEntityException, NotFoundException, NotImplementedException} from "@nestjs/common";
import { firstValueFrom } from "rxjs";
import { jwtDecode } from "jwt-decode";
import { jwtProofService } from "src/services/proof-services/jwt-proof.service";
import { get_rules, isVP, retrieveVCinfo, retrieveVCinVPJWT } from "src/utils";
import * as jp from 'jsonpath'
import { HttpService } from "@nestjs/axios";
import { uuid } from "uuidv4";
import { ComplianceGaiaX, ComplianceCredentialSubject } from "src/dto";
import { getDID } from "src/utils";

export class sdJWTProvider implements credentialProvider {
    constructor(private type:string) {}
    logger = new Logger("sd-JWTProvider")
    proofService = new jwtProofService()
    httpService = new HttpService()
    async parse(vp:string): Promise<VerificationPayload> {
        this.logger.log("parsing for sdjwt")
        let verificationPayload:VerificationPayload = {
          rules:[]
        }
        let requirements = []
        let promises: Promise<void>[] = []
        let parsedVCs: any[] = []
        await this.verify(vp)
        let rules = get_rules(this.type)

          rules.map(rule => {
            if(rule.required){
              rule.required.type.map(type => {
                if(!requirements.includes(type)) {
                    requirements.push(type)
                }
            })
            }     
          })
        retrieveVCinVPJWT(vp).map(x => {
          parsedVCs.push(retrieveVCinfo(x))
        })
        console.log(parsedVCs)
        requirements.map(async requirement => {
              promises.push(this.findVC(parsedVCs, requirement, verificationPayload))
            })

        await Promise.all(promises)

        return verificationPayload
    }

    async verify(vp: string ): Promise<void> {
      let signatureResults: testResult[] = []
        let signaturePromises:Promise<testResult>[] = []
        let vcs:any[] =  retrieveVCinVPJWT(vp)
        if(!isVP(vp)) {
          throw new BadRequestException("Submitted JWT is not a Verifiable Presentation of type application/vp+jwt")
        }
        signaturePromises.push(this.proofService.validate(vp))
        vcs.map(x => {
          signaturePromises.push(this.proofService.validate(x))
        })
        signatureResults = await Promise.all(signaturePromises)
        await this.checkResult(signatureResults)
  
  
      }
  

    async createComplianceCredential(
      complianceGX: ComplianceGaiaX<ComplianceCredentialSubject>,
      type:string,
      vcID?:string,
      credentialSubjectID?:string
    ) {
      let credentialPromise:Promise<any>;
      if(complianceGX.credentialSubject['gx:type']==='gx:LegalParticipant' && type==='Participant') {
        credentialPromise = this.createMembershipAsterX(complianceGX.credentialSubject)

    } else {
      throw new NotImplementedException("Currently only Membership is supported for sd-jwt")
    }
    return await Promise.resolve(credentialPromise)
    }


      async checkResult(signatureResult: testResult[]):Promise<void> {
        let failedSignature: string[] = []
          signatureResult.map(x => {
            if(x.result===false) {
              failedSignature.push(x.id)
            }
          })
          if(failedSignature.length) throw new BadRequestException("Signature is invalid for credential of id : " + failedSignature)
       }


       async findVC(vcs, requirement, verificationPayload) {
        try {
          let result = await new Promise<void>((resolve, reject) => {
            let searchForTypeInVC = jp.query(vcs, '$[*].credentialSubject.type')
            let searchForTypeInVCld =jp.query(vcs,'$[*].credentialSubject["@type"]')
            let searchForTypeInArray = jp.query(vcs, '$[*].credentialSubject[*].type')
            if(searchForTypeInVC.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type ${requirement}`)
              let vc = jp.query(vcs, `$[?(@.credentialSubject.type==="${requirement}")]`)[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else if(searchForTypeInVCld.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type ${requirement}`)
              let vc = jp.query(vcs, `$[?(@.credentialSubject["@type"]==="${requirement}")]`)[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else if(searchForTypeInArray.find(x => x===requirement)) {
              this.logger.log(`Matching vc for type in searchForTypeInArray "${requirement}"`)
              let ArrayVC = jp.query(vcs, `$[?(@.credentialSubject[0])]`)
              let vc = ArrayVC.filter(x => x.credentialSubject.some(subject => subject.type === requirement))[0]
              verificationPayload[`${requirement}`] = vc
              resolve()
            }
            else {
              reject("unprocessableEntity")
            }
          })
        } catch(e) {
          console.log(e)
          throw new UnprocessableEntityException(`Missing VC of type ${requirement}`)
        }    
      }


      private async createMembershipAsterX(participantCompliance) {
        try {
          let uriEncoded = encodeURIComponent(participantCompliance.id)
          let participantVC = (await firstValueFrom(this.httpService.get(process.env.VERIFIER + "/api/resolve/" + uriEncoded))).data
          let vcID = process.env.USER_AGENT + "/organization/" + uuid()  + "/data.json"
          let credentialSubjectID = process.env.USER_AGENT + "/organization/" + uuid()  + "/data.json"
          let subjectID = participantVC.issuer
          let federation = process.env.FEDERATION || "Aster-X"
          let credentialSubject={
              "@id": credentialSubjectID,
              "@type": "vcard:Organization",
              "vcard:title": `${federation} Membership`,
              "vcard:hasMember": {
                "@type": "vcard:Individual",
                "vcard:fn": participantVC.credentialSubject["gx:legalName"],
                "aster-conformity:hasDid": subjectID
          }
        }
        let toBeSigned = {
          "@context":[
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
         ],
          "@id":vcID,
          issuer:getDID(),
          credentialSubject:credentialSubject,
          type:["VerifiableCredential"]
        }
        let membership = (await firstValueFrom(this.httpService.put(process.env.VC_ISSUER+"/api/v0.9/sign", toBeSigned ))).data
          return membership
      
        
        } catch(e) {
          console.error(e)
          if(e.response.status===404) {
            console.error(e.response)
            throw new NotFoundException("Participant credential in Gaia-X Compliance is not resolvable")
          } 
          if(e.response.status===400) {
            console.error("throwing bad request")
            throw new BadRequestException(e.response.data.msg)
          } else {
            console.error(e)
          }
        }
      }
  
  
      // private async createServiceOfferingAsterX(serviceCompliance) {
      //   try {
      //     let uriEncoded = encodeURIComponent(serviceCompliance.id)
      //     let serviceOfferingVC = (await firstValueFrom(this.httpService.get(process.env.VERIFIER + "/api/resolve/" + uriEncoded))).data
      //     let vcID = process.env.USER_AGENT + "/organization/" + uuid()  + "/data.json"
      //     let credentialSubjectID = process.env.USER_AGENT + "/organization/" + uuid()  + "/data.json"
      //     let federation = process.env.FEDERATION || "AsterX"
      //     let credentialSubject={
      //       "@id": credentialSubjectID,
      //       "@type": `aster-conformity:${federation}Compliance`,
      //       "aster-conformity:hasGaiaXCompliance":serviceCompliance.id,
      //       "aster-conformity:serviceName":serviceOfferingVC.credentialSubject["gx:name"]
      //     }
  
      //     let toBeSigned = {
      //       "@context":[
      //         "https://www.w3.org/2018/credentials/v1",
      //         "https://w3id.org/security/suites/jws-2020/v1",
      //         "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
      //      ],
      //       "@id":vcID,
      //       issuer:getDID(),
      //       credentialSubject:credentialSubject,
      //       type:["VerifiableCredential"]
      //     }
  
      //     let asterXCompliance = (await firstValueFrom(this.httpService.put(process.env.VC_ISSUER+"/api/v0.9/sign", toBeSigned ))).data
      //     return asterXCompliance

       
      
        
      //   } catch(e) {
      //     console.error(e)
      //     if(e.response.status===404) {
      //       console.error(e.response)
      //       throw new NotFoundException("Participant credential in Gaia-X Compliance is not resolvable")
      //     } 
      //     if(e.response.status===400) {
      //       console.error("throwing bad request")
      //       throw new BadRequestException(e.response.data.msg)
      //     } else {
      //       console.error(e)
      //     }
      //   }
      // }

}