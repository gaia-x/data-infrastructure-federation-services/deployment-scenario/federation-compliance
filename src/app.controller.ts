import { Controller, Get, Param, Post, Body, HttpCode, Query, Header, Req, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags, ApiResponse, ApiOperation,  ApiParam, ApiBody, ApiQuery, ApiConsumes, ApiSecurity} from '@nestjs/swagger';
import {
  AllRulesDescription,
  CredentialSubjectDto,
  InitiateSession,
  RulesDescription, testResult, VerifiableCredentialDto, VerifiablePresentationDto
} from './dto'
import { VerifiablePresentationCheckDto } from './dto/vpcheck.dto';
import * as VP from './static/example.json'
import * as jwtExample from './static/example-jwt-sd.json'
import * as soJSONLD from './static/exampleSO.json'
import { SignatureService } from './services/signature.service';
import { credentialProvider } from './interface/credential.interface';
import { ApiKeyGuard } from './auth/apikey.guard';



@ApiTags("aster-x compliance ")
@Controller({ path: '/api/' })
export class AppController {
  constructor(private readonly appService: AppService, private readonly signatureService: SignatureService ) {}

  @ApiResponse({
    status: 200,
    description: 'A list of all supported rules and their description'
  })
  @ApiOperation({
    summary:
      'Search for all the rules supported in this abc-checker version'
  })
  @Get("/rules")
  getRules(): AllRulesDescription {
    return this.appService.getAllRules();
  }

  @ApiResponse({
    status: 200,
    description: 'The rule searched and its description'
  })
  @ApiResponse({
    status: 404,
    description: 'Rules not found in list of supported rules'
  })
  @ApiOperation({
    summary:'Search for a specific rule in this abc-checker version'
  })
  @ApiParam({
    name:"id",
    required:true,
    type: 'string',
    description: 'ID of the rules searched',
    example:"rule_01"
  })
  @ApiQuery({
    name:"type",
    required:true,
    type: 'enum',
    description: 'type of compliance needed',
    enum:["Participant", "ServiceOffering"]
  })
  @Get("/rules/:id")
  getRule(@Param("id") id, @Query("type") type):RulesDescription {
    return this.appService.getRules(id, type)
  }


  @ApiResponse({
    status: 200,
    description: 'The VP has passed the tests'
  })
  @ApiResponse({
    status: 403,
    description: 'One or more test has failed '
  })
  @ApiResponse({
    status: 422,
    description: 'Missing needed VC in VP'
  })
  @ApiOperation({
    summary:'Apply a certain amount of checks to a VP'
  })
  @ApiConsumes('application/vp+sd-jwt')
  @ApiConsumes('application/vp+jwt')
  @ApiConsumes('application/json')
  @ApiBody(
    {
    type: VerifiablePresentationDto,
    examples: {
      jsonld: {
        value:VP
      },
      SDJWTBad: {
        value:jwtExample.sdBad
      },
      SDJWTGood: {
        value:jwtExample.sdGood
      },
      JWTGood: {
        value:jwtExample.vcjwtGood
      }
    },

  })


  @HttpCode(200)
  @Post("/credential-offer/membership")
  async checkRuleMembership(@Body() payload: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>, @Req() req ):Promise<VerifiableCredentialDto<CredentialSubjectDto>> {
    let headers = req.headers['content-type']
    let type = "Participant"
    this.appService.checkRequest(headers, payload)
    let provider: credentialProvider = this.appService.getProvider(headers,type)
    let verificationPayload = await provider.parse(payload)

    await this.appService.checkRules(verificationPayload, type)
    let complianceCredential = await  provider.createComplianceCredential(verificationPayload["gx:compliance"], type)
    console.log(complianceCredential)
    return complianceCredential
  } 



  @ApiResponse({
    status: 200,
    description: 'The VP has passed the tests'
  })
  @ApiResponse({
    status: 403,
    description: 'One or more test has failed '
  })
  @ApiResponse({
    status: 422,
    description: 'Missing needed VC in VP'
  })
  @ApiOperation({
    summary:'Apply a certain amount of checks to a VP'
  })

  @ApiConsumes('application/vp+sd-jwt')
  @ApiConsumes('application/vp+jwt')
  @ApiConsumes('application/json')
  @ApiQuery({
    name:"vcid",
    description:"optionnal parameter to define the ID of the issued VC, if none is specified then the component will generate an id and expose the service on the webserver of the federation",
    required:false,
    allowEmptyValue:true
  })
  @ApiQuery({
    name:"credentialSubjectid",
    description:"optionnal parameter to define the ID of the credential subject, if none is specified then the component will generate an id and expose the service on the webserver of the federation",
    required:false,
    allowEmptyValue:true
  })
  @ApiBody(
    {
    type: VerifiablePresentationDto,
    examples: {
      JSONLD: {
        value:soJSONLD
      }
    },
  })
  @HttpCode(200)
  @ApiSecurity('X-API-KEY')
  @UseGuards(ApiKeyGuard)
  @Post("/credential-offer/service")
  async checkRuleService(@Body() payload: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>, @Req() req , @Query("vcid") vcID, @Query("credentialSubjectid") credentialSubjectID):Promise<VerifiableCredentialDto<CredentialSubjectDto>> {
    let headers = req.headers['content-type']
    let type = "ServiceOffering"
    this.appService.checkRequest(headers, payload)
    console.log(headers)
    console.log(payload)
    let provider: credentialProvider = this.appService.getProvider(headers,type)
    let verificationPayload = await provider.parse(payload)
    await this.appService.checkRules(verificationPayload, type)
    let complianceCredential = await  provider.createComplianceCredential(verificationPayload["gx:compliance"], type, vcID, credentialSubjectID)
    return complianceCredential
  } 
  





  @ApiOperation({
    summary:'Gerenerate a challenge and display the domain associated with this verifier'
  })
  @HttpCode(200)
  @Post("/credential-offer/initiate")
  InitiateCredentialOffer():InitiateSession {
    return this.appService.initiateSession()
  }






  
}
