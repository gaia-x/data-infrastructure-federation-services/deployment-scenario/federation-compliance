import { Logger, UnprocessableEntityException } from "@nestjs/common";
import { jwtDecode } from "jwt-decode";
import { createHash } from "crypto";
import { DecodedVC, DecodedVP, DisclosureVerification } from "src/dto";

const logger = new Logger()
export function retrieveVCinVPJWT(vp:string): any[] {
    let decodedVP:DecodedVP = jwtDecode(vp)
    if(decodedVP.verifiableCredential) {
        return decodedVP.verifiableCredential
    }
    if(decodedVP.vcs) {
        return decodedVP.vcs
    } 
    if(decodedVP.vp) {
        if(decodedVP.vp.vc){
            return decodedVP.vp.vc
        } 
        if(decodedVP.vp.verifiableCredential) {
            return decodedVP.vp.verifiableCredential
        }
        
    } else {
        throw new UnprocessableEntityException("Cannot retrieve vc")
    }
}

export function isVP(vp:string):boolean {
    let decodedVP:DecodedVP = jwtDecode(vp)
    if(decodedVP.verifiableCredential || decodedVP.vcs || decodedVP.vp) {
        return true
    } else {
        return false
    }
}


export function retrieveVCinfo(vc:string | any ) {
    if(typeof(vc) === "string") {
        if(vc.split("~").length>1){
            return retrieveVCinfo(validateAllDisclosure(vc))
        } else {
            let decodedVC:DecodedVC = jwtDecode(vc)
            if(decodedVC.vc) {
                return retrieveVCinfo(decodedVC.vc)
            } else {
                return retrieveVCinfo(decodedVC)
            }
        }
    } else {
        if(vc.issuer["id"]) {
            let reformatedIssuer = vc.issuer["id"]
            delete vc.issuer
            vc.issuer = reformatedIssuer
            return vc 
        } else {
            return vc
        }
    }
}


export function validateAllDisclosure(vc:any) {
    let deconstructedVC: string[] = vc.split("~")
    let vcWithoutDisclosure = jwtDecode(deconstructedVC[0])
    let disclosure = deconstructedVC.slice(1,deconstructedVC.length)
    let listOfDisclosureHash: DisclosureVerification [] = []
    logger.log("Begin processing of disclosure")
    disclosure.map(x =>  {
        if(checkDisclosureValidity(x)) {
            listOfDisclosureHash.push({
                field:JSON.parse(Buffer.from(x,'base64').toString())[1],
                value:JSON.parse(Buffer.from(x,'base64').toString())[2],
                hash:calculateHashFromDisclosure(x),
                isInVC:false
            })
        }
    })
    logger.log("Hash computed, now searching for matching in submitted VC")
    searchForDisclosure(vcWithoutDisclosure, listOfDisclosureHash)
    logger.log("Checking if every disclosure have been allocated")
    checkDisclosureList(listOfDisclosureHash)
    return vcWithoutDisclosure
}


function calculateHashFromDisclosure(disclosure:string) {
    let disclosurePreHash:Uint8Array = Buffer.from(disclosure, 'utf-8');
    let shabyte:Uint8Array=  createHash('sha256').update(disclosurePreHash).digest()
    let lensha = shabyte.byteLength;
    let hash:string = ""
    for (var i = 0; i < lensha; i++) {
        hash += String.fromCharCode( shabyte[ i ] );
    }
    let hashInVC = btoa(hash).replace(/=+$/,'').replace(/\+/g,'-').replace(/\//g,'_')
    return hashInVC
}


function searchForDisclosure(object:any, disclosure:DisclosureVerification[]){
    let keys = Object.keys(object)
    keys.map(x => {
        if(typeof(object[x])==="object") {
            if(x==="_sd") {
                validateDisclosure(object,disclosure)
            } else {
                searchForDisclosure(object[x], disclosure)
            } 
        } 
    })

}


function validateDisclosure(object:any, listOfDisclosure:DisclosureVerification[]) {
    listOfDisclosure.map(disclosure => {
        object["_sd"].map(hash => {
            if(hash==disclosure.hash) {
                if(!disclosure.isInVC){
                    if(Object.keys(object).includes(disclosure.field)) {
                        throw new UnprocessableEntityException(`Disclosure cannot be processed due to: trying to disclose an already existing field`)
                    } 
                    object[disclosure.field] = disclosure.value
                    disclosure.isInVC=true
                } else {
                    throw new UnprocessableEntityException(`Disclosure cannot be processed due to: Two digest are the same within the credential`)
                }
            }
        })
    })
    delete object["_sd"]

}


function checkDisclosureList(disclosureList:DisclosureVerification[]) {
    disclosureList.map(disclosure => {
        if(!disclosure.isInVC) {
            throw new UnprocessableEntityException("Some disclosure revealed by the Holder are not located in the corresponding VC, VC cannot be processed")
        }
    })
}


function checkDisclosureValidity(disclosure: string):boolean {
    let parsedDisclosure = JSON.parse(Buffer.from(disclosure,'base64').toString())
    if(parsedDisclosure.length!==3) {
        throw new UnprocessableEntityException(`Disclosure ${disclosure} cannot be processed due to: not respecting the format of disclosure`)
    }
    if(parsedDisclosure[1] ==="..." || parsedDisclosure[1]==="_sd") {
        throw new UnprocessableEntityException(`Disclosure ${disclosure} cannot be processed due to: useing _sd or ... as field name`)
    }
    return true
}