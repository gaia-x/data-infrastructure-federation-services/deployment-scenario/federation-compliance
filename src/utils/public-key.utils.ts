import { writeFileSync } from 'fs'
import { join } from 'path'

export function importCertChain() {

    const X509_CERTIFICATE_CHAIN_FILE_PATH = join(__dirname, '../../src/static/.well-known/x509CertificateChain.pem')
    writeFileSync(X509_CERTIFICATE_CHAIN_FILE_PATH, process.env.publicKey)
  
}
