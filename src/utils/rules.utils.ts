import { readFileSync } from 'fs'

export function get_rules(type:string) {
    let path = process.env.RULES_PATH
    let fragment=""
    if(type==="Participant") {
        fragment="participant"
    } else if(type==="ServiceOffering") {
        fragment="service-offering"
    }   
    const rules = readFileSync(path + `/rules-${fragment}.json`, 'utf-8')
    return JSON.parse(rules)
}


export function get_all_rules() {
    let path = process.env.RULES_PATH

    const rulesParticipant = readFileSync(path + `/rules-participant.json`, 'utf-8')
    const rulesServiceOffering = readFileSync(path + `/rules-service-offering.json`, 'utf-8')

    return {
        rulesParticipant:JSON.parse(rulesParticipant),
        rulesServiceOffering:JSON.parse(rulesServiceOffering)
    }
}