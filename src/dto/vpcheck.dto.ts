import { VerifiablePresentationDto, VerifiableCredentialDto, CredentialSubjectDto } from "./";
import { ApiProperty } from '@nestjs/swagger'


export abstract class VerifiablePresentationCheckDto {
    @ApiProperty({
        description: 'Verifiable Presentation which needs checks'
      })
      vp:VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>


    
}