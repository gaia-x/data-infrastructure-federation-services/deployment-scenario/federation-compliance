import { ApiProperty } from '@nestjs/swagger'
import { SignatureDto } from './signature.dto'
import { CredentialSubjectDto, VerifiableCredentialDto } from './vc.dto'

export abstract class VerifiablePresentationDto<T extends VerifiableCredentialDto<CredentialSubjectDto>> {
  @ApiProperty({
    description: 'The context to be used for the verifiable presentation.'
  })
  public '@context': string[] | any

  @ApiProperty({
    description: 'The type of verifiable presentation.'
  })
  public '@type': string[]

  @ApiProperty({
    description: 'The identifier of the self description.',
    required: false
  })
  public '@id'?: string

  @ApiProperty({
    description: 'The verifiable credential included in the VP'
  })
  public verifiableCredential: T[]

  @ApiProperty({
    description: 'proof',
    required: false
  })
  public proof: SignatureDto
}


