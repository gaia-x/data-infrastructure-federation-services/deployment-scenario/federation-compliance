
import { JwtPayload } from "jwt-decode";


export interface DecodedVP extends JwtPayload {
    vp?: VPContent
    verifiableCredential?: any[]
    vcs?: any[]
}




interface VPContent {
    vc?:string[]
    verifiableCredential?: any[]
}