import { ApiProperty } from "@nestjs/swagger";

export class DisclosureVerification {
    @ApiProperty({
        description:"key which is hidden"
    })
    field:string
    @ApiProperty({
        description:"value which is hidden"
    })
    value:string
    @ApiProperty({
        description:"hash corresponding to the key and value hidden"
    })
    hash:string

    @ApiProperty({
        description:"check whether the disclosure has been allocated in the VC"
    })
    isInVC:boolean
}

