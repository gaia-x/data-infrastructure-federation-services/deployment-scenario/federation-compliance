
import { JwtPayload } from "jwt-decode";
import { CredentialSubjectDto, VerifiableCredentialDto } from "./vc.dto";


export interface DecodedVC extends JwtPayload {
    vc:VerifiableCredentialDto<CredentialSubjectDto>
    
}