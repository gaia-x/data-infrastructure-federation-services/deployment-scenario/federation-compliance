import { ApiProperty } from '@nestjs/swagger'

export class Session {
  @ApiProperty({
    description: 'index of the session'
  })
  challenge: string

  @ApiProperty({
    description: 'Time to live'
  })
  ttl: number


}
