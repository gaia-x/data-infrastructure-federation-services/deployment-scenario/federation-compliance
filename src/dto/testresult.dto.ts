import { ApiProperty } from '@nestjs/swagger'

export class testResult {
  @ApiProperty({
    description: 'Id of the rule'
  })
  rule_id: string

  @ApiProperty({
    description: 'Error message'
  })
  message: string

  @ApiProperty({
    description: 'Error message'
  })
  result: boolean | any


  @ApiProperty({
    description: 'Error message'
  })
  id?: string
}
