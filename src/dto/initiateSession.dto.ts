import { ApiProperty } from '@nestjs/swagger'

export class InitiateSession {
  @ApiProperty({
    description: 'index of the session'
  })
  challenge: string

  @ApiProperty({
    description: 'Verifier unique stinrg'
  })
  domain:string


}
