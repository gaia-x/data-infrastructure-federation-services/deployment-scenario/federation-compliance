import { ApiProperty } from '@nestjs/swagger'

export class RulesDescription {
  @ApiProperty({
    description: 'Id of the rule'
  })
  id: string

  @ApiProperty({
    description: 'description of the rule'
  })
  description: string
}

export class RulesRequirementDto {
    @ApiProperty({
        description: 'Id of the rule'
      })
      id: string
    
    @ApiProperty({
        description: 'required type for a rule to be applied'
      })
      required: {
        type:string[]
      }

    @ApiProperty({
        description: 'optionnal type for a rule to be applied'
      })
    optional?: {
    type:string[]
    }
}


export class AllRulesDescription {
  @ApiProperty({
    description: 'Rules Participant'
  })
  rulesParticipant: RulesDescription
  @ApiProperty({
    description: 'Rules SO'
  })
  rulesServiceOffering: RulesDescription
}