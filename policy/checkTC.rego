package play

import future.keywords.if



default main := false

get_tc(url) := res {
	http_response := http.send({
		"url": url + "/api/termsAndConditions",
		"method": "get"
	})
    res := http_response.body
    print(res)
}


main{
	input["abc:TermsAndConditions"].credentialSubject.type !="abc:TermsAndConditions"
} else {
	tc:= input["abc:TermsAndConditions"].credentialSubject["gx:termsAndConditions"]
    federationTC:=get_tc(input.registryURL)
    tc=="The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n\nThe keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD)."
}

