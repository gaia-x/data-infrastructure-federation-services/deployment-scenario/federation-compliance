<h1 align="center">Aster-X Compliance</h1>



## Description

This project is a component that can be used to apply a set of rules to a GX Participant or their service in order to let them access an ecosystem through the use of w3c Verifiable Credentials (Such as here Aster-X).

It revolves arround the usage of [Open-Policy-Agent](https://www.openpolicyagent.org/) to apply policy to a specific Verifiable Credential such as for example, requiring in the payload a valid GXDCH Compliance credential 

This component is to be used with GXFS-FR component, it uses both GXFSF-FR User-agent for storing and exposing VC as well as GXFS-FR VC-issuer on the case where the user wants to expose his credential on his servers. You will find more about those component on those pasges [user-agent](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/-/tree/develop/agent/api/views?ref_type=heads) [vc-issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/-/tree/develop/deployment/packages/overlays?ref_type=heads).

The components also uses the Aster-X Verifier in order to apply technical verification to any credential received.

While being similar to GXDCH Clearing house compliance component in usage, this composant revolves around the usage of [JSON Path](http://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/intro/) to parse any incoming VP depending on the chosen rules.

This component is expected to extend GXDCH rules. Therefore, implementors MUST include a way to identify credentials issued by a GXDCH, whether its compliance for Participant Credential or Service-Offering. Extended rules MAY include the verification of extended GXDCH Schemas, ecosystem-limited Terms and Condition or rules applied to specific fields (such as geolocalisation restrictions).

To define new Terms And Condition or have an extended list of Trusted Issuers, implementors MAY use [GXFSFR Ecosystem Registry](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/gxfsfr-dev/src?ref_type=heads)


Please note that this project is a prototype and may be subject to architectural modification and is not considered as a production ready component 

### Manage your own Rules

To add or manage your Rules, you will need to specify the route of your rules repository (or the volume associated). In this repository, you will need to create two files which describes the rules used by your component. Those files must be named rules-participant.json and rules-service-offering.json and must follow this format : 

```json
[
    {
        "id":"rule_0X",
        "description":"Description of the rules",
        "required": {
            "type":["credentialType A"]
        }
    },
    {
        "id":"rule_0Y",
        "description":"Description of the rule",
        "required": {
            "type":["credentialType B"],
            "registry":true,
            "path":"/api/some/path"
        }
    }
]
```

In this format, two fields are optionnal: registry and path. 
Those fields allow you to communicate to your OPA in a normalized way if need to communicate with your ecosystem registry (env variable). When used, it will send along your VCs the variable "rules_0Y:registryURL" the URL + path mentionned in the rule

For the type attribute. For the moment, it only support the type mentionned in CredentialSubject (whether the credential subject is an object or an array)

## Installation

### Prerequisite

To use this project, you will need to have a OPA server running on localhost (on your machine or as a side car in an infrastructure). To recreate the demonstrator of GXFSFR, you may use [OPAL](https://docs.opal.ac/) and set your data source to [this repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/abc-checker)

```bash
$ yarn install
```

You will also need to have a User-agent and a VC-Issuer, or any compenent with the same api design
## Setup the environment variables

To run this application you need to setup few environment variables with examples located in the file .env.example:

- privateKey : The key used by the component to emmit a Verifiable Credential
- publicKey : The certificate chain associated with the private key you defined, it is mostly used to generate the DID Document of the component and expose the certificate chain to allow further verification
- REGISTRY_URL: The url of the registry used by the component to perform authorization verification defined in your rules. You could use Aster-X registry or your own implementation
- RULES_PATH: The location of your rules file, it will be used by the component to load the rules you defined for the Aster-X Compliance 
- MODE: Whether you want to run the application in dev mode or production mode (PRODUCTION). It enables the security mecanism on Verifiable Presentation
- DOMAIN: A security parameter which is to be included in Verifiable Presentation as a way to prevent slight possibility of colision with 2 identical challenges 
- API_KEY_COMPLIANCE: To limit access to the service offering compliance to only those who are known by the federation, an API key has been created and is intended to be locked behind an API Gateway of the federation
- VC_ISSUER: The url to your signing mecanism, you could use a personal one as long as it has the same route 
- USER_AGENT:  The url to the user agent of your federation
- USER_AGENT_API_KEY: The API key of the user agent of the federation


## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Use Ecosystem Compliance

### Get all rules

Using the `/api/rules` route, you can get all current rules you defined previously as mandatory to pass the ecosystem compliance 


**Responses:**

If rules are found , a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
{
  "rulesParticipant": [
    {
      "id": "rule_0X",
      "description": "Check element",
      "required": {
        "type": [
          "SomeCredential"
        ],
        "registry": true,
        "path": "/some/path/in/registry"
      }
    }
  ],
  "rulesServiceOffering": [
    {
      "id": "rule_0Y",
      "description": "Check element",
      "required": {
        "type": [
          "SomeCredential"
        ],
        "registry": true,
        "path": "/some/path/in/registry"
      }
    }
  ]
}
```


### Get a specific rule

Using the `/api/rules/{id}?type=` route, you can get a specific rule for a participant or a service offering. The parameter id is corresponding to the id field of a rule you defined in your rules files, The query type is either Participant or ServiceOffering


**Responses:**

If a rules is found , a HTTP response status code `200` and a response object will be returned.

If none, a HTTP response status code `404` will be returned

**Response object:**

HTTP response status code `200`:

```json

  {
      "id": "rule_0X",
      "description": "Check element",
      "required": {
        "type": [
          "SomeCredential"
        ],
        "registry": true,
        "path": "/some/path/in/registry"
      }
    }

```


### Get Membership

Using the `/api/credential-offer/membership` route, you can submit a Verifiable Presentation to the compliance service to get Membership to the ecosystem. The route will check whether your VP contains all necessary VCs according to the rules you previously defined and send them to your authorization server.

If all rules are passed, a compliance credential will be emitted and exposed using the User-Agent

If the MODE environnement variable is set to PRODUCTION, two properties must be included in the Verifiable Presentation Proof object: a challenge and a domain. Domain is managed by the environment variable DOMAIN and challenge is a random generated string that is valid for five minutes after its issuance (and if it has not been used before)

Both element can be retrieved using the `/api/credential-offer/initiate` route

Note that the component support jsonld, vc-jwt and sd-jwt

**Request body in JSON:**

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "@id": "did:web:abc-federation.gaia-x.community",
  "@type": [
    "verifiablePresentation"
  ],
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1"
      ],
      "id": "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
      "type": [
        "VerifiableCredential",
        "LegalPerson"
      ],
      "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
      "credentialSubject": {
        "type": "LegalPerson",
        "id": "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        "gx-participant:registrationNumber": {
          "gx-participant:registrationNumberType": "local",
          "gx-participant:registrationNumberNumber": "0762747721"
        },
        "gx-participant:headquarterAddress": {
          "gx-participant:addressCountryCode": "IT",
          "gx-participant:addressCode": "IT-25",
          "gx-participant:streetAddress": "Via Europa",
          "gx-participant:postalCode": "20121"
        },
        "gx-participant:legalAddress": {
          "gx-participant:addressCountryCode": "IT",
          "gx-participant:addressCode": "IT-25",
          "gx-participant:streetAddress": "Via Europa",
          "gx-participant:postalCode": "20121"
        }
      },
      "expirationDate": "2023-06-21T12:17:52.960Z",
      "issuanceDate": "2023-04-27T12:57:35.063542+00:00",
      "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "created": "2023-04-27T12:57:35.063542+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..IPCszweqbC91zSJMD0ymX1O4DQsRN76heRQHpdw6Qt7kG_z_GaSXWp72hPg5wjw9OHraR26TubJCnd8yxtE26JEBeynSdXJE1howrnN9vOdqCsM-hBBFwQWANhWbvhuG6dWZeSPb9_sHK2_SQZF9kyOSqDOes80jWy0chhGsk-oBKBy0Mco4xu3sOb3BtkSUD1FfrPby1fi_AjHyDBEwmpWevQ_zd5-CVBNh6jnP4VPQqyWuzEZefwWismCvNaKPeKQKNjo1qEl5VZ2bcEXDbl7LQeSN7OMhquw1tdLHaNkK3QijNxS3_k7qpjzrgKhgzUQN2KH2h0bYb1ECUIfkYA"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "challenge":"52055c86-cff0-48ad-bde2-4fb445ca02a1",
    "domain":"Aster-X-Compliance",
    "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..i-GonxH3OJZnvCMr5bimoEVawzz--H4F41ns0WFKMuYRaPV25ygia_g45AmyuTmWHLyOaSnUC761-lO9NrlswU5wEsJ9MNiYUMAwSIOU-0nh-_eVguGvosi3sZj5mWeStjxM6f9dwUm4EOnzRnWBDdbD7-IkHPg6QFOzZzEazza9DJ35hFxgkz6sYBoRuuRbtezULsZ0mY6kNTRGce6NthyLsMfHX74ETSgKQI9j3anwp2YfUF7XX9f40Qz4AIVEXIp0wiND_E7WwBSsYuBtY1a2aAGBTrgG2_i2SjsbkOz5GmoyoZViUqBtW2ehS59uRFE5zg2WW5SdL262NthH7w"
  }
}
```

**Responses:**

If all the rules are passed, a response code `200` will be returned as well as a compliance credential depending on the type of compliance you asked.

**Response object:**

HTTP response status code `200`:

```json
{
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@id": "https://aster-x.demo23.gxfs.fr/organization/db5ce470-1f01-4fbb-b17d-6b1fb367b4b5/data.json",
  "@type": [
    "VerifiableCredential"
  ],
  "credentialStatus": {
    "id": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/aster-x-revocation#1240",
    "statusListCredential": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/aster-x-revocation",
    "statusListIndex": "1240",
    "statusPurpose": "revocation",
    "type": "StatusList2021Entry"
  },
  "credentialSubject": {
    "@id": "https://aster-x.demo23.gxfs.fr/organization-json/db5ce470-1f01-4fbb-b17d-6b1fb367b4b5/data.json",
    "@type": "vcard:Organization",
    "vcard:hasMember": {
      "@type": "vcard:Individual",
      "aster-conformity:hasDid": "did:web:dufourstorage.provider.dev.gaiax.ovh",
      "vcard:fn": "Dufour Storage"
    },
    "vcard:title": "Aster-X Membership"
  },
  "expirationDate": "2024-11-30T09:53:48.769059+00:00",
  "issuanceDate": "2024-06-03T09:53:48.769059+00:00",
  "issuer": "did:web:aster-x.demo23.gxfs.fr",
  "proof": {
    "created": "2024-06-03T09:53:48.769059+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..SoV-dErIfcTAL6NGCL_1DynrKgk1J0TCpxSXC2rPTljHABi0rb6SAGnzjfuE6CMaEYDtOgJNl5rmCAyf06L1NbNMFDXk2XoCEU-u_JtcOm1S1s7QJhLblyW2YJCtp6PHaz2saSKbXxs7ieAn3JJOLwsWB9sYoWvJp7UoyV03hJ6g6j0A8p4o6QECiiHpUEuh55BAIWgrP5OdW1HFlToLQJ_2Kc8R8yTTWAY5gLNAIcpClF9E0EZkGAdCZYdlzC4MLMx5KiRq8rUZbZW9LaTs6klPzF8RQI0Ykd6KdGzZS_8aFNoqTVQm-_31HtnuOARpMycnYrEqFILLKSx5ztz9OQ",
    "proofPurpose": "assertionMethod",
    "type": "JsonWebSignature2020",
    "verificationMethod": "did:web:aster-x.demo23.gxfs.fr#JWK2020-RSA"
  }
}
```

**Errors**

If one or more test failed, an error `403` will be raised and a response object will be returned

If one or more VC is missing from the payload (according to the rules previously defined), an error `422` will be raised and a response object will be returned 

**Response object:**

HTTP response status code `403`:

```json
{
  "statusCode": 403,
  "message": [
    {
      "description": "SomeRule",
      "result": false,
      "rule_id": "rule_0X"
    }
  ],
  "error": "Forbidden"
}
```

HTTP response status code `422`:

```json
{
  "statusCode": 422,
  "message": "Missing VC of type abc:TermsAndConditions",
  "error": "Unprocessable Entity"
}
```






### Certify a Service

Using the `/api/credential-offer/service` route, you can submit a Verifiable Presentation to the compliance service to get a compliance credential for this service. The route will check whether your VP contains all necessary VCs according to the rules you previously defined and send them to your authorization server.

If all rules are passed, a compliance credential will be emitted. If you enter value for parameter credentialSubjectid and vcid, the credential will be signed using the ecosystem VC-Issuer, otherwise the ID will be generated by the component and exposed by the ecosystem User-Agent.

This route is protected by an API Key to limit access to those who are authenticated within the federation using the API Gateway

If the MODE environnement variable is set to PRODUCTION, two properties must be included in the Verifiable Presentation Proof object: a challenge and a domain. Domain is managed by the environment variable DOMAIN and challenge is a random generated string that is valid for five minutes after its issuance (and if it has not been used before)

Both element can be retrieved using the `/api/credential-offer/initiate` route

Note that the component support jsonld, vc-jwt and sd-jwt

**Request body:**

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "@type": [
        "VerifiablePresentation"
    ],
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1",
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            ],
            "type": [
                "VerifiableCredential"
            ],
            "id": "https://dufourstorage.provider.dev.gaiax.ovh/gaiax-service-offering/685616f5c27aca08b039f7723720db02ba1de3b59581c48b0c62266d9a15fbc5/data.json",
            "issuer": "did:web:compliance.lab.gaia-x.eu:v1-staging",
            "issuanceDate": "2024-05-30T08:09:39.029Z",
            "expirationDate": "2024-08-28T08:09:39.029Z",
            "credentialSubject": [
                {
                    "type": "gx:compliance",
                    "id": "https://dufourstorage.provider.dev.gaiax.ovh/service-offering/685616f5c27aca08b039f7723720db02ba1de3b59581c48b0c62266d9a15fbc5/data.json",
                    "gx:integrity": "sha256-0e6ab412d0f943c6c891c9217541c167a063d64777037ef267183ea0e5496cb8",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:ServiceOffering"
                },
                {
                    "type": "gx:compliance",
                    "id": "https://dufourstorage.provider.dev.gaiax.ovh/legal-participant/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json",
                    "gx:integrity": "sha256-49366eb6b61ef88ebe11e3481270d8cee643d44700ef7addd8445334344e42b4",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:LegalParticipant"
                },
                {
                    "type": "gx:compliance",
                    "id": "https://dufourstorage.provider.dev.gaiax.ovh/gaiax-terms-and-conditions/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bldp6dcc67c0eda3/data.json",
                    "gx:integrity": "sha256-f66bc654ae64d422991cd855a666e858b7bd0fe83fe84d2fb66d5d0a1cc93942",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:GaiaXTermsAndConditions"
                },
                {
                    "type": "gx:compliance",
                    "id": "https://dufourstorage.provider.dev.gaiax.ovh/gaiax-legal-registration-number/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json",
                    "gx:integrity": "sha256-9c097a67dd8fd10ef07f5295dbf119f185d4fcb7166a4855fd98fda8fad3b756",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:legalRegistrationNumber"
                }
            ],
            "proof": {
                "type": "JsonWebSignature2020",
                "created": "2024-05-30T08:09:39.390Z",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:compliance.lab.gaia-x.eu:v1-staging#X509-JWK2020",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..k6OwfYpFJkDXSLKjPOwiqY3Y8A_ggZQRhqP0S6JGIpaKbfseWJOI3be__qIl_h8KkYcN4vUGM2OgsU2e_60s5bnMh_5979k--x6YtGDkwKqyMtvowPL3iG4Z6mqwIoyj8gX-hnAdQTYzcWuDoLe-gG36WcdW7fVLn96rsHJlObi_3PrMnbfyblj_8x5NsJpl3gHGMIRf5Jc0lbngul22S-6AqaKLqdP08BZgrS00wjq3LzdMhDTTx_0EfEG4UtxyLmXil0re0zUKSz9IvSOoIc9HsQd9Payt8QDOonB6_HMTbdFlS_BqqA9xgsj9SsueqyaKYtA6c3MCCe-skEY-wA"
            }
        }
    ],
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh#JWK2020-RSA",
        "created": "2024-06-03T08:16:19.208162+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..konEAH1vvHSYkFfj_4qksbKFZfeK0tl6SrALtOmYGcPwpXyXPQmZeXJDxCrIX46WA30SwxwlQSoAdQ_RH8xy8v-yrwwc2tiNY75-_Vcp9YrD96FIgm2xGiWvvw_iXn-9IfUYCwFyKPRdAWiRDiMCr2kyk4gvjVVLf-0ekYNqXazgI_bdTQuVWHiMMnVIEo-M_iqIKf9pjxMLOaI7X4Xu56GNSHKc_ps4mtQzTZYLMQqUoj4ccsDEhBLvnrQ_BrmUWi6rdSlDAoEUdjUX3OwMP55l9USMHM5z3s1Wp8c4tCszsp8XaWmpw7rn5fdghgwPzrUQwAeRnHZaZ8cjVe504w"
    }
}
```

**Responses:**

If all the rules are passed, a response code `200` will be returned as well as a compliance credential depending on the type of compliance you asked.

**Response object:**

HTTP response status code `200`:

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://w3id.org/security/suites/jws-2020/v1",
    "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
  ],
  "@id": "https://foobar",
  "issuer": "did:web:abc-federation.dev.gaiax.ovh",
  "credentialSubject": {
    "@id": "https://foobar",
    "@type": "aster-conformity:AsterXCompliance",
    "aster-conformity:hasGaiaXCompliance": "https://dufourstorage.provider.dev.gaiax.ovh/service-offering/685616f5c27aca08b039f7723720db02ba1de3b59581c48b0c62266d9a15fbc5/data.json",
    "aster-conformity:serviceName": "Backup Service"
  },
  "type": [
    "VerifiableCredential"
  ],
  "expirationDate": "2024-11-30T08:50:12.871782+00:00",
  "issuanceDate": "2024-06-03T08:50:12.871782+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:abc-federation.dev.gaiax.ovh#JWK2020-RSA",
    "created": "2024-06-03T08:50:12.871782+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..R8njObx35c0Dx6SZA7ylj0OT8WoaT8zJbJaVGa4f2NkS2Xx4ReANtWqU_TIMPlYaKGEdKNkGN-SvUhAX3zBUjrrZ0Ahxjs7_8ss43okudspNeatvEv4FMBLMagyEPro1D33FW3DHc0wHeEBgRRgap0ykLMvC4UPRS6oRmxBJamQ3p12_rTZIFDpyyNdfGECYhZmtExfsewD-fl8OsH3jrVj4-ddJaT_43MAspl1hM1cniMeKPEwk59KNV4hF0ugAXXLCL51VwtrwQg8WDIa4VPNVkUAdlVsPsJv7sO-UqmdIhJKlgLLkyHvUGvM1rOY74W5jRBVZ97CWqXTimR0BUQ"
  }
}
```

**Errors**

If one or more test failed, an error `403` will be raised and a response object will be returned

If one or more VC is missing from the payload (according to the rules previously defined), an error `422` will be raised and a response object will be returned 

**Response object:**

HTTP response status code `403`:

```json
{
  "statusCode": 403,
  "message": [
    {
      "description": "SomeRule",
      "result": false,
      "rule_id": "rule_0X"
    }
  ],
  "error": "Forbidden"
}
```

HTTP response status code `422`:

```json
{
  "statusCode": 422,
  "message": "Missing VC of type abc:TermsAndConditions",
  "error": "Unprocessable Entity"
}
```


### Component Workflow


```mermaid
sequenceDiagram
  participant Holder
  participant Compliance
  participant AsterX
  Holder->>Compliance: Ask for challenge and domain (/initiate)
  Note right of Compliance: Production Only
  Compliance->> Holder: Return challenge (unique) and domain (reusable)
  Holder ->> Holder: Sign Verifiable Presentation using challenge and domain
  Holder ->> Compliance: Send Verifiable Presentation
  Compliance ->> Compliance: Verify challenge and domain (production only)
  Note right of Compliance: After Verification, challenge is considered obsolete and must be regenerated for another api call
  Compliance ->> Compliance: Analyse mediaType of the request
  Compliance ->> Compliance: Verify signature of every credential depending on the mediaType 
  Note right of Compliance: Compliance uses Aster-X Verifier in the case of application/json 
  Compliance ->> Compliance: Parse the credentials to create a payload understandable by OPA Server
  Compliance ->> OPA: Send payload for verification
  OPA ->> OPA: Apply rules defined in Aster-X Compliance
  OPA ->> Compliance: Submit result
  Compliance ->> AsterX: Ask for credential 
  AsterX ->> Compliance: Return signed credential (exposed or not depending on parameter)
  Compliance ->> Holder: Send Membership / Compliance Service
```

  







